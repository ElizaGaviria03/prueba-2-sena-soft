package com.eliza.prueba2senasoft.Adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.eliza.prueba2senasoft.R;

import java.util.ArrayList;


public class UserAdapter extends RecyclerView.Adapter<UserAdapter.ViewHolderUser> {

    ArrayList<ClaseEliza> listUser;

    public UserAdapter(ArrayList<ClaseEliza> listUser) {
        this.listUser = listUser;
    }

    @Override
    public ViewHolderUser onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_user,null,false);

        return new ViewHolderUser(view);
    }

    @Override
    public void onBindViewHolder(ViewHolderUser holder, int position) {

        holder.item_name.setText(listUser.get(position).getName());
        holder.item_username.setText(listUser.get(position).getUsername());
        holder.item_phone.setText(listUser.get(position).getPhone());
        holder.item_email.setText(listUser.get(position).getEmail());


    }

    @Override
    public int getItemCount() {
        return listUser.size();
    }

    public class ViewHolderUser extends RecyclerView.ViewHolder {

        TextView item_name,item_username,item_email,item_phone;
        public ViewHolderUser(View itemView) {
            super(itemView);

            item_name = (TextView) itemView.findViewById(R.id.item_name);
            item_username = (TextView) itemView.findViewById(R.id.item_username);
            item_phone = (TextView) itemView.findViewById(R.id.item_phone);
            item_email = (TextView) itemView.findViewById(R.id.item_email);
        }
    }
}
