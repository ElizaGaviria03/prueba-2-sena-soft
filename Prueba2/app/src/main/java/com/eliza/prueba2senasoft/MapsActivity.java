package com.eliza.prueba2senasoft;

import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.widget.Button;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    double LatLng;
    Button maps_btn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        // Add a marker in Sydney and move the camera
        LatLng Gwenborough = new LatLng(-37.3159, 81.1496);
        mMap.addMarker(new MarkerOptions().position(Gwenborough).title("Marker in Gwenborough"));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(Gwenborough));

        LatLng Wisokyburgh = new LatLng(-43.9509, -34.4618);
        mMap.addMarker(new MarkerOptions().position(Wisokyburgh).title("Marker in Wisokyburgh"));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(Wisokyburgh));

        LatLng McKenziehaven = new LatLng(-68.6102, -47.0653);
        mMap.addMarker(new MarkerOptions().position(McKenziehaven).title("Marker in McKenziehaven"));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(McKenziehaven));

        LatLng SouthElvis = new LatLng(29.4572, -164.2990);
        mMap.addMarker(new MarkerOptions().position(SouthElvis).title("Marker in South Elvis"));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(SouthElvis));

        LatLng Roscoeview = new LatLng(-31.8129, 62.5342);
        mMap.addMarker(new MarkerOptions().position(Roscoeview).title("Marker in Roscoeview"));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(Roscoeview));
    }
}
